extends "res://_scripts/templates/agent.gd"

# Movement Parameters to be used in different goals
export var slow_approach_speed := 75.0
export var walk_speed := 200.0
export var run_speed : = 300.0

export var patrol_wait_time := 3000.0
export var investigate_wait_time := 1000.0

var max_wait_time = 0.0

export var patrol_approach_distance := 50.0
export var investigate_approach_distance := 50.0
export var retreat_approach_distance := 100.0
export var offensive_approach_distance := 50.0
export var safe_distance := 200.0

var max_approach_distance := 0.0

export var attack_range := 90
var is_attack_complete := false

# Reference to map configuration to calculate path
export (NodePath) var navigation_path
onready var navigation = get_node(navigation_path)
export (NodePath) var graph_path
onready var graph = get_node(graph_path)

# Reference to draw path planning for debug
export (NodePath) var line_path
onready var line = get_node(line_path)

# Reference to graph nodes for patrol routine
var previous_patrol_point : Position2D
var next_patrol_point : Position2D

var target

# Time for reaction baloon
export var ballon_reaction_time := 2.0
var ballon_time := 0.0

# Used to transit animation
onready var animation_state = $AnimationTree.get("parameters/playback")

func _ready():
	randomize()
	$goal_thinker.init(self)
	
func _process(delta):
	$goal_thinker.process()
	$perception_memory.process_perception()
	update_ballon(delta)
	
	# Animation parameters update
	$AnimationTree.set("parameters/idle/blend_position", heading)
	$AnimationTree.set("parameters/walk/blend_position", heading)
	$AnimationTree.set("parameters/attack/blend_position", heading)
	
	# If attack animation is occuring, don't change animation
	if !is_attack_complete:
		return
		
	# Chagne animation dependeing on velocity
	if velocity.length_squared() < 1:
		animation_state.travel("idle")
	else:
		animation_state.travel("walk")
	
	
func get_perception_memory() -> Node:
	return $perception_memory
	
func is_goal_completed() -> bool:
	return $goal_thinker.is_completed()
	
# Receive signal from perception
func _on_perception_memory_sensed_something(target):
	if get_squad_status() == SquadEnum.SQUAD_STATUS.OFFENSIVE:
		return
		
	ballon_time = ballon_reaction_time
	$goal_thinker.push_wait_reaction(ballon_reaction_time * 1000, target)
	$curious.visible = true
	pass
	
# Basically a timer for the ballon reaction
func update_ballon(delta):
	if ballon_time > 0:
		ballon_time -= delta
		if ballon_time < 0:
			$surprised.visible = false
			$curious.visible = false


#############################
#
#	Controls for Goal Thinker
#
#############################

func set_heading_with_velocity():
	if velocity.length_squared() > 1.0:
		heading = velocity.normalized()

# Set heading to a target position
func look_at(target : Vector2):
	heading = (target - position).normalized()

# Get a patrol point from graph
func get_next_patrol_point(must_be_unvisited = false) -> Position2D:
	# Clear previous patrol point, now that it's going for another one
	if previous_patrol_point != null:
		previous_patrol_point.is_targeted = false
		previous_patrol_point.visited = true
		
	next_patrol_point = null
	# Set previous point as the closest node the agent is
	previous_patrol_point = graph.nodes[graph.astar.get_closest_point(position)]
	# Get candidates for next patrol point
	var potential_next_points = graph.astar.get_point_connections(previous_patrol_point.id)
	
	while next_patrol_point == null:
		# Return null if no candidate was found
		var num_points = potential_next_points.size()
		if potential_next_points.size() <= 0:
			return null
		
		# Select a random point connected to closes patrol point the guard is
		var potential_point_index = randi() % num_points
		var potential_point = potential_next_points[potential_point_index]
		# If free, set as next point, or else remove from candiates
		if graph.nodes[potential_point].is_targeted || (must_be_unvisited && graph.nodes[potential_point].visited ):
			potential_next_points.remove(potential_point_index)
		else:
			graph.nodes[potential_point].is_targeted = true
			next_patrol_point = graph.nodes[potential_point]
			
	return next_patrol_point

# Calculate path from current point to another point
func calculate_path(to : Vector2) -> PoolVector2Array:
	if line != null:
		line.points = navigation.get_simple_path(position, to)
	#current_path = $Navigation2D.get_simple_path(get_owner().position, to)
	return navigation.get_simple_path(position, to)

# Calculate path from current point to a node in the graph
func calculate_path_to_node(to : int) -> PoolVector2Array:
	if line != null:
		line.points = navigation.get_simple_path(position, graph.nodes[to].position)
	#current_path = $Navigation2D.get_simple_path(get_owner().position, to)
	return navigation.get_simple_path(position, graph.nodes[to].position)
	
# Get connected nodes from graph for a specific node 
func get_connected_points(id : int) :
	var connected_points = graph.astar.get_point_connections(id)
	var connected_nodes = []
	for i in range(connected_points.size()):
		connected_nodes.push_back(graph.nodes[connected_points[i]])
		
	return connected_nodes
	
# Calculate danger when approaching target
func arbitrate_danger() -> float:
	$fuzzy_module_danger.fuzzify($fuzzy_module_danger/variables/fuzzy_variable_direction, heading.dot(target.heading))
	$fuzzy_module_danger.fuzzify($fuzzy_module_danger/variables/fuzzy_variable_distance, (target.position - position).length())
	
	return ($fuzzy_module_danger.defuzzify($fuzzy_module_danger/variables/fuzzy_variable_danger))

# Control max speed of guard
func set_run():
	max_speed = run_speed
func set_walk():
	max_speed = walk_speed
func set_slow_approach():
	max_speed = slow_approach_speed

# Perform an attack by starting animation
func attack():
	is_attack_complete = false
	animation_state.travel("attack")
	return
	
# Called by Animation Player to indicate end of animation and transit
func finish_attack():
	animation_state.travel("idle")
	is_attack_complete = true
	$spear.visible = false

################################
#
#	Controls for squad
#
################################

# Set guard to patrol
func set_patrol():
	$goal_thinker.set_patrol()
	
# Set guard to investigate determined path
func set_investigate(path):
	$goal_thinker.set_investigate(path)

# Set guard to offensive stance
func set_offensive(formation_position, target):
	self.target = target
	$goal_thinker.set_offensive(formation_position, target)
	
	if $curious.visible:
		$curious.visible = false
		$surprised.visible = true
		ballon_time = ballon_reaction_time
	
# Set guard to retreat to an exit
func retreat(exit) :
	$goal_thinker.retreat(exit)

# Set guard to keep guarding an exit
func set_guard_exit(exit):
	$goal_thinker.guard_exit(exit)

# Get Squad FSM State
func get_squad_status():
	return get_parent().get_parent().current_status
	
