extends "res://_scripts/templates/agent.gd"

export (NodePath) var navigation_path
onready var navigation = get_node(navigation_path)

var sound_source = null

func process_perception():	
	$perception_memory.process_perception()
	
	if $perception_memory.has_seen_target:
		if $perception_memory.is_target_in_fov:
			$steering_behaviour.target_object = $perception_memory.last_seen_target
			$steering_behaviour.arrive_on = false
			$steering_behaviour.intercept_on = true
		else:
			$steering_behaviour.target_position = $perception_memory.last_seen_position
			$steering_behaviour.intercept_on = false
			$steering_behaviour.arrive_on = true
	else:
		$steering_behaviour.arrive_on = false
		$steering_behaviour.intercept_on = false
		
	if $perception_memory.has_heard_target:
		heading = ($perception_memory.last_heard_position - position).normalized()
	else:
		heading = Vector2(1.0, 0.0)

		
