extends KinematicBody2D

var target_position = null
var speed = 150
var velocity = Vector2(0.0, 0.0)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _physics_process(delta):
	if target_position != null :
		velocity = ((target_position - position).normalized() * speed)
		position +=  velocity * delta
		
	if (position - get_node("../agent_perception").position).length_squared() < 500 * 500:
		get_node("../agent_perception/perception_memory").process_sound(position)


func _unhandled_input(event):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT:
			position = event.position
			target_position = null
		if event.button_index == BUTTON_RIGHT:
			target_position = event.position
