extends "res://_scripts/templates/agent.gd"

export(NodePath) var target
export(NodePath) var target2
export(NodePath) var target3
export(NodePath) var target4
export(NodePath) var target5

func _ready():
	$steering_behaviour.target_object = get_node(target)
	$steering_behaviour.target_object2 = get_node(target2)
	$steering_behaviour.target_position = get_node(target).position
	#$steering_behaviour.path.push_back(get_node(target).position)
	#$steering_behaviour.path.push_back(get_node(target2).position)
	#$steering_behaviour.path.push_back(get_node(target3).position)
	#$steering_behaviour.path.push_back(get_node(target4).position)
	#$steering_behaviour.path.push_back(get_node(target5).position)
	
	
func _physics_process(delta):
	._physics_process(delta)
	$steering_behaviour.target_position = get_node(target).position
