extends Sprite

var target_position = null
var speed = 150
var velocity = Vector2(0.0, 0.0)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _physics_process(delta):
	if target_position != null :
		velocity = ((target_position - position).normalized() * speed)
		position +=  velocity * delta
