extends Node2D


	
func _input(event):
	# Mouse in viewport coordinates
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT:
			$target.position = event.position
		if event.button_index == BUTTON_RIGHT:
			$target.target_position = event.position
