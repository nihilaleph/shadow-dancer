extends "res://_scripts/templates/goal.gd"

var current_target
#onready var goal_follow_path = load("res://_scripts/goal_follow_path.gd")

func activate():
	current_status = GOAL_STATUS.ACTIVE
	return
	
func process() -> int:
	activate_if_inactive()
	
	if process_subgoals() == GOAL_STATUS.FAILED:
		remove_all_subgoals()
		go_to_position(current_target)
	
	process_subgoals()
		
	return GOAL_STATUS.ACTIVE
	
func terminate():
	return

func go_to_position(target: Vector2) :
	current_target = target
	agent_owner.calculate_path(current_target)
	var subgoal = preload("res://_scripts/tests/goal_driven/goal_follow_path.gd").new()
	subgoal.init(agent_owner)
	subgoal.current_path = Array(agent_owner.current_path)
	subgoals.push_front(subgoal)
	
