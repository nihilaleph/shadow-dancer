extends "res://_scripts/templates/goal.gd"

var target : Vector2
var is_last_target := false
var time_expected : float
var start_time : float
var is_stuck := false
var acceptable_distance := 50

func init(agent):
	.init(agent)

func activate():
	
	current_status = GOAL_STATUS.ACTIVE
	
	start_time = OS.get_ticks_msec()
	time_expected = start_time + ((target - agent_owner.position).length() / agent_owner.max_speed) * 1000
	# Add 2sec margin error
	time_expected += 2000.0
	agent_owner.get_steering().target_position = target
	
	if is_last_target:
		agent_owner.get_steering().arrive_on = true
	else :
		agent_owner.get_steering().seek_on = true
	return
	
func process() -> int:
	activate_if_inactive()
	
	if OS.get_ticks_msec() > time_expected:
		current_status = GOAL_STATUS.FAILED
	elif (target - agent_owner.position).length() < acceptable_distance:
		current_status = GOAL_STATUS.COMPLETED
	
	return current_status
	
func terminate():
	agent_owner.get_steering().seek_on = false
	agent_owner.get_steering().arrive_on = false
	return
