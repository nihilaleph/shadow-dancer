extends "res://_scripts/templates/agent.gd"

export (NodePath) var navigation_path
export (NodePath) var tilemap
onready var navigation = get_node(navigation_path)

var current_path : PoolVector2Array

func _ready():
	#$Navigation2D.navpoly_add(get_node(tilemap), Transform2D.IDENTITY)
	$goal_thinker.init(self)

func _unhandled_input(event):
	if event is InputEventMouseButton:
		
		if event.button_index == BUTTON_RIGHT:
			var to = event.position
			$goal_thinker.go_to_position(to)

func calculate_path(to : Vector2):
	#current_path = $Navigation2D.get_simple_path(get_owner().position, to)
	current_path = navigation.get_simple_path(position, to)
	
func _process(delta):
	$goal_thinker.process()
