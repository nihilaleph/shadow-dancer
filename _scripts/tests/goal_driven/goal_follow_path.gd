extends "res://_scripts/templates/goal.gd"

var goal_move_to
var current_path

func activate():
	current_status = GOAL_STATUS.ACTIVE
	goal_move_to = preload("res://_scripts/tests/goal_driven/goal_move_to.gd")
	
	if !current_path.empty():
		var target = current_path.pop_front()
		
		var new_goal = goal_move_to.new()
		new_goal.init(agent_owner)
		new_goal.target = target
		new_goal.is_last_target = current_path.empty()
		
		add_subgoal(new_goal)
		
	return
	
func process() -> int:
	activate_if_inactive()
	
	current_status = process_subgoals()
	
	if is_completed() && !current_path.empty():
		activate()
		
	return current_status
	
func terminate():
	remove_all_subgoals()
	return

func add_subgoal(goal : Node):
	subgoals.push_front(goal)
