extends Node2D
signal give_me_food

# Miner Proprieties
var gold_in_pocket = 0
var gold_in_bank = 0
var energy = 10
var cooldown = 0
var has_food = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _process(delta):
	cooldown += delta
	if cooldown > 1 :
		cooldown = 0
		$fsm_miner.execute()

func deposit_gold():
	gold_in_bank += gold_in_pocket
	gold_in_pocket = 0
	print("I have " + str(gold_in_bank) + " gold in the bank!")
	
func ask_for_food():
	emit_signal("give_me_food")
	
func eat() -> bool:
	if has_food:
		print("food!")
		has_food = false
		return true
	else:
		print("Where is my food?!")
		return false
		
func _on_elsa_food_is_ready():
	has_food = true
