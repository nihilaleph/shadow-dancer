extends Node2D

signal food_is_ready

var cooking = false
var husband_arrived = false
var cooking_cooldown = 0
var cooking_time = 2
var bladder = 0
var cooldown = 0

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _process(delta):
	cooldown += delta
	if cooking:
		cooking_cooldown += delta
	if cooldown > 1 :
		cooldown = 0
		$fsm_elsa.execute()

func deliver_food():
	cooking = false
	cooking_cooldown = 0
	emit_signal("food_is_ready")


func _on_miner_give_me_food():
	husband_arrived = true
