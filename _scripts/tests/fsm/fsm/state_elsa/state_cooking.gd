extends "res://_scripts/templates/state.gd"

func enter(agent):
	print("Oh boy, gotta cook for my husband")
	return

func execute(agent):
	print("cooking cooking...")
	agent.cooking = true
	if agent.cooking_cooldown > agent.cooking_time:
		agent.deliver_food()
		get_owner().revert_state()
	return

func exit(agent):
	print("There you go!")
	return
