extends "res://_scripts/templates/state.gd"

func enter(agent):
	return

func execute(agent):
	if agent.husband_arrived:
		get_owner().change_state(get_node("../state_cooking"), true)
		agent.husband_arrived = false
	return

func exit(agent):
	return
