extends "res://_scripts/templates/state.gd"

func enter(agent):
	print("Time to clean")
	return

func execute(agent):
	print("Cleaning Cleaning...")
	agent.bladder += 1
	if agent.bladder > 7:
		get_owner().change_state(get_node("../state_bathroom"))
	return

func exit(agent):
	print("Enough of cleaning!")
	return
