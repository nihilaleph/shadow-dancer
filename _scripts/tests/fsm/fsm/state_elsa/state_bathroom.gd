extends "res://_scripts/templates/state.gd"

func enter(agent):
	print("Gotta take a leak")
	return

func execute(agent):
	print("ahhh....")
	agent.bladder -= 1
	if agent.bladder <= 0 :
		get_owner().change_state(get_node("../state_housework"))
	return

func exit(agent):
	print("Much better!")
	return
