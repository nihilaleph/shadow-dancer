extends "res://_scripts/templates/state.gd"

func enter(agent):
	print("Entering house!")
	agent.ask_for_food()
	return

func execute(agent):
	if agent.eat():
		get_owner().change_state(get_node("../state_rest"))
	return

func exit(agent):
	print("So full!")
	return
