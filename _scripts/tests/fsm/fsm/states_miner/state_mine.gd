extends "res://_scripts/templates/state.gd"

func enter(agent):
	print("Entering the mine!")	
	
func execute(agent):
	print("Diggin some gold")
	if randf() > .7:
		agent.gold_in_pocket += 1
		print("Lucky! Got some gold!")
	agent.energy -= 1
	if agent.energy <= 0:
		agent.deposit_gold()
		get_owner().change_state(get_node("../state_eating"))
	
	
func exit(agent):
	print("Exiting mine...")
