extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _unhandled_input(event):
	if event is InputEventKey:
		if event.pressed:
			var hp = randi() % 100
			var distance = randi() % 500
			$fuzzy_module_danger.fuzzify($fuzzy_module_danger/variables/fuzzy_variable_hp, hp)
			$fuzzy_module_danger.fuzzify($fuzzy_module_danger/variables/fuzzy_variable_distance, distance)
			
			print(str(hp) + " " + str(distance))
			print($fuzzy_module_danger.defuzzify($fuzzy_module_danger/variables/fuzzy_variable_danger))
			
