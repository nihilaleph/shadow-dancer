extends "res://_scripts/templates/state.gd"

export var visible_limit := 1000.0
export var audible_limit := 2000.0
export var bank_limit := 2000.0

# Set guards on patrol
func enter(agent):
	agent.current_status = SquadEnum.SQUAD_STATUS.PATROL
	agent.set_guards_patrol()
	return

func execute(agent):
	# If intruder was seen during the limit time, change guards to offensive formation
	if agent.intruder_visible_time > visible_limit:
		get_owner().change_state(get_node("../state_offensive"))
		return
	# If intruder was heard for the limit time, or enough guards sensed it
	# in different occasions, call for a warning
	if agent.intruder_audible_time > audible_limit || agent.intruder_perception_time_bank > bank_limit:
		get_owner().change_state(get_node("../state_warning"))
		return

func exit(agent):
	return
