extends "res://_scripts/templates/state.gd"

# Set guards offensive formation position
func enter(agent):
	agent.current_status = SquadEnum.SQUAD_STATUS.OFFENSIVE
	agent.set_formation_position(agent.intruder.position)
	agent.set_guards_offensive()
	return

func execute(agent):
	agent.set_formation_position(agent.intruder.position)
	
	# Call guards to retreat to exit in case intruder is not visible anymore
	if !agent.intruder_has_been_seen || agent.intruder_invisible:
		get_owner().change_state(get_node("../state_retreat"))
		
	return

func exit(agent):
	return
