extends "res://_scripts/templates/goal.gd"

export var visible_limit := 500.0

# Set guards to retreat to an exit
func enter(agent):
	agent.current_status = SquadEnum.SQUAD_STATUS.RETREAT
	agent.set_guards_retreat()
	return

func execute(agent):
	# If intruder was seen during the limit time, change guards to offensive formation
	if agent.intruder_visible_time > visible_limit:
		get_owner().change_state(get_node("../state_offensive"))
		return
	# When agents completed their retreat, initiate defensive formation
	if agent.get_guards_completed():
		get_owner().change_state(get_node("../state_defensive"))
	return

func exit(agent):
	return
