extends "res://_scripts/templates/state.gd"

export var visible_limit := 500.0
export var warning_time := 10000.0
var warning_timeout = 0.0

# Set guards to warning formation and set timeout of the warning
func enter(agent):
	agent.current_status = SquadEnum.SQUAD_STATUS.WARNING
	agent.set_formation_position(agent.intruder.position)
	agent.set_guards_warning()
	warning_timeout = OS.get_ticks_msec() + warning_time
	return

func execute(agent):
	# If intruder was seen during the limit time, change guards to offensive formation
	if agent.intruder_visible_time > visible_limit:
		get_owner().change_state(get_node("../state_offensive"))
		return
	# If the warning time is over, return to patrol state
	if warning_timeout < OS.get_ticks_msec():
		agent.reset_guards_warning()
		get_owner().change_state(get_node("../state_patrol"))
		

func exit(agent):
	return
