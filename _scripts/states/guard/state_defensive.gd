extends "res://_scripts/templates/state.gd"

export var visible_limit := 500.0

# Set guards to investigate area
func enter(agent):
	agent.current_status = SquadEnum.SQUAD_STATUS.DEFENSIVE
	agent.set_guards_investigate()
	return

func execute(agent):
	# Go to offensive if intruder has been seen for longer than the limit
	if agent.intruder_visible_time > visible_limit:
		get_owner().change_state(get_node("../state_offensive"))
		return
	# If guards finished their routine, restart formation
	if agent.get_guards_completed():
		get_owner().change_state(get_node("../state_defensive"))
	return

func exit(agent):
	return
