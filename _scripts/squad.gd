# Class to coordinate the squad of Guards
# Retains a Finite State Machine to control tactics used by the squad
extends Node2D

# Class to sort guards according to their distance to a determined position
class GuardPositionSorter:
	var position
	func sort(a, b):
		if (a.position - position).length_squared() < (b.position - position).length_squared():
			return true
		return false

# Cooldown to execute FSM in seconds
export var fsm_cooldown := 0.5
var cooldown := 0.0

# Enum to keep up with the current squad status (mirrors FSM)
var current_status : int

# Variables controlling perception of the whole squad
# To be used by FSM

# If intruder still not identified but enough time has passed sensing him
# Will trigger alert
var intruder_perception_time_bank := 0.0

# Sum time of intruder visible for all guards
var intruder_visible_time := 0.0
# Sum time of intruder audible for all guards
var intruder_audible_time := 0.0

var intruder_in_sight := false
var intruder_has_been_seen := false
var intruder : Node2D
# Player can become invisible
var intruder_invisible := false

# Hard coded investigation path that guards must follow
# Only hardcoded because sometimes export reset them
export var investigate_path := [[2, 8, 15, 16, 13, 12, 9, 5, 6, 0],\
								[17, 11, 4, 3, 6, 7, 10, 14, 13, 1]]


# Subsquads division to control determined group or singular guards to perform duties
var subsquads : Array

var picked_guards : Array

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

func _process(delta):
	cooldown += delta
	if cooldown > fsm_cooldown:
		get_guards_perception()
		$fsm_squad.execute()
		cooldown = 0.0

# Compile guards perception memory and digest the information
func get_guards_perception():
	intruder_visible_time = 0.0
	intruder_audible_time = 0.0
	intruder_perception_time_bank = 0.0
	intruder_in_sight = false
	intruder_has_been_seen = false
	for guard in $guards.get_children():
		if guard.get_perception_memory().has_seen_target:
			intruder_visible_time += guard.get_perception_memory().get_delta_time_visible()
			intruder = guard.get_perception_memory().last_seen_target
			
		if guard.get_perception_memory().has_heard_target:
			intruder_audible_time += guard.get_perception_memory().get_delta_time_audible()
		
		# Perception bank don't care if it is being seen or heard now
		intruder_perception_time_bank += guard.get_perception_memory().get_delta_time_audible()
		intruder_perception_time_bank += guard.get_perception_memory().get_delta_time_visible()
		
		# Check if someone saw intruder
		intruder_in_sight = intruder_in_sight || guard.get_perception_memory().is_target_in_lof
		intruder_has_been_seen = intruder_has_been_seen || guard.get_perception_memory().has_seen_target
		
	if intruder != null:
		intruder_invisible = intruder.is_invisible		

###################################
#
#	Guard control functions
#
###################################

# Set all guards to patrol
func set_guards_patrol():
	for guard in $guards.get_children():
		guard.set_patrol()
	
# Pick a guard from each subsquad that wasnt picked last time and set it to investigate
func set_guards_investigate():
	var exits = get_tree().get_nodes_in_group("exit_nodes")
	var picking_guards = []
	for i in range(subsquads.size()):
		if subsquads[i].size() > 1:
			var picked_guard = null
			while picked_guard == null:
				picked_guard = subsquads[i][randi() % subsquads[i].size()]
				if picked_guards.find(picked_guard) >= 0:
					picked_guard = null
					
			picking_guards.push_back(picked_guard)
			picked_guard.set_investigate(investigate_path[i])
			
		# Guards not investigating should guard exit
		for guard in subsquads[i]:
			if picking_guards.find(guard) < 0:
				guard.set_guard_exit(exits[i])
			
	picked_guards = picking_guards
	
# Set all guards to retreat considering their distance to each exit
func set_guards_retreat():
	var exits = get_tree().get_nodes_in_group("exit_nodes")
	
	var guards = $guards.get_children()
	
	var guard_sorter = GuardPositionSorter.new()
	guard_sorter.position = exits[0].position
	guards.sort_custom(guard_sorter, "sort")
	
	var proportion = guards.size() / exits.size()
	
	# Create a subsquad per exit
	subsquads = []
	for i in range(proportion):
		subsquads.push_back([])
		
	# Populate subsquads
	for i in range(guards.size()):
		guards[i].retreat(exits[i / proportion])
		subsquads[i / proportion].push_back(guards[i])
		

# Set one guard to retreat to each exit considering their distance
func set_guards_warning():
	picked_guards = []
	var exits = get_tree().get_nodes_in_group("exit_nodes")
	
	var guards = $guards.get_children()
	
	for exit in exits:
		var guard_sorter = GuardPositionSorter.new()
		guard_sorter.position = exit.position
		guards.sort_custom(guard_sorter, "sort")
		
		var searching_for_guard = true
		var i = 0
		while searching_for_guard:
			if picked_guards.find(guards[i]) < 0:
				guards[i].retreat(exit)
				picked_guards.push_back(guards[i])
				searching_for_guard = false
		
# Reset guards on exit guarding duties to patrol again
func reset_guards_warning():
	for guard in picked_guards:
		guard.set_patrol()
	picked_guards = []

# Set all guards to offensive formation considering their distance to each position
func set_guards_offensive():
	# Check where the exits are
	var exits = get_tree().get_nodes_in_group("exit_nodes")
	
	var exit_gravity_center = Vector2(0.0, 0.0)
	for exit in exits:
		exit_gravity_center += exit.position
	exit_gravity_center /= exits.size()
	
	var guards = $guards.get_children()
	var pick_guard

	# Prioritize formation position according to intruder position
	if intruder.position.y >  exit_gravity_center.y:
		pick_guard = pick_guard_for_formation_position(guards, 0)
		pick_guard.set_offensive($formation.get_child(0), intruder)
		guards.remove(guards.find(pick_guard))
		pick_guard = pick_guard_for_formation_position(guards, 1)
		pick_guard.set_offensive($formation.get_child(1), intruder)
		guards.remove(guards.find(pick_guard))
	else :
		pick_guard = pick_guard_for_formation_position(guards, 1)
		pick_guard.set_offensive($formation.get_child(1), intruder)
		guards.remove(guards.find(pick_guard))
		pick_guard = pick_guard_for_formation_position(guards, 0)
		pick_guard.set_offensive($formation.get_child(0), intruder)
		guards.remove(guards.find(pick_guard))
		
	if intruder.position.x >  exit_gravity_center.x:
		pick_guard = pick_guard_for_formation_position(guards, 2)
		pick_guard.set_offensive($formation.get_child(2), intruder)
		guards.remove(guards.find(pick_guard))
		pick_guard = pick_guard_for_formation_position(guards, 3)
		pick_guard.set_offensive($formation.get_child(3), intruder)
		guards.remove(guards.find(pick_guard))
	else :
		pick_guard = pick_guard_for_formation_position(guards, 3)
		pick_guard.set_offensive($formation.get_child(3), intruder)
		guards.remove(guards.find(pick_guard))
		pick_guard = pick_guard_for_formation_position(guards, 2)
		pick_guard.set_offensive($formation.get_child(2), intruder)
		guards.remove(guards.find(pick_guard))
	
# Place formation node on top of intruder
func set_formation_position(position : Vector2):
	$formation.position = position

# Select from an array of guards the closes to a specific formation position
func pick_guard_for_formation_position(guards : Array, formation_index : int) -> Node2D:
	var guard_sorter = GuardPositionSorter.new()
	guard_sorter.position = $formation.get_child(formation_index).global_position
	guards.sort_custom(guard_sorter, "sort")
	
	return guards[0]

# Check if all guards completed their duties
func get_guards_completed() -> bool:
	var completed = true
	for guard in $guards.get_children():
		completed = completed && guard.is_goal_completed()
		
	return completed
