extends Area2D

# Checks if player is inside torch radius and illuminates it
func _physics_process(delta):
	for body in get_overlapping_bodies():
		# Check if there is Line of Sight to body
		$RayCast2D.enabled = true
		$RayCast2D.cast_to = body.position - global_position
		if $RayCast2D.is_colliding():
			body.illuminate(self, false)
		else:
			body.illuminate(self, true)

# Called whenever the player leaves the torch radius
func _on_torch_body_exited(body):
	body.illuminate(self, false)
	$RayCast2D.enabled = false
