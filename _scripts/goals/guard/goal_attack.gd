# Activate agent owner attack function

extends "res://_scripts/templates/goal.gd"


func init(agent):
	agent_owner = agent

# Initiate attack
func activate():
	current_status = GOAL_STATUS.ACTIVE
	agent_owner.attack()
	return
	
func process() -> int:
	activate_if_inactive()
	
	# Complete goal after attack was completed
	if agent_owner.is_attack_complete:
		current_status = GOAL_STATUS.COMPLETED
	return current_status
	
func terminate():
	return
