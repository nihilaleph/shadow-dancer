# Keep a guard in an exit point and cycle watch to connected graph nodes

extends "res://_scripts/templates/goal.gd"

var exit

func init(agent, exit):
	agent_owner = agent
	self.exit = exit

func activate():
	current_status = GOAL_STATUS.ACTIVE
	# Set up movement parameters for agent
	agent_owner.set_run()
	agent_owner.max_wait_time = agent_owner.patrol_wait_time
	agent_owner.max_approach_distance = agent_owner.patrol_approach_distance
	
	populate_subgoals()
	return
	
func process() -> int:
	activate_if_inactive()
	
	# If succeeded, just restart goal
	if is_completed():
		activate()
		
	current_status = process_subgoals()
	
	return current_status
	
func terminate():
	remove_all_subgoals()
	return
	
	
func populate_subgoals():
	var subgoal_wait = preload("res://_scripts/goals/guard/goal_wait.gd")
	var next_points = agent_owner.get_connected_points(exit.id)
	var subgoal
	
	# Make agent watch for all connected graph nodes to the exit
	
	for i in range(next_points.size()):
		subgoal = subgoal_wait.new()
		subgoal.init(agent_owner, agent_owner.max_wait_time, next_points[i])
		subgoals.push_front(subgoal)
		
		
	# If agent become too far from exit for some reason, add goal to come back
	if (exit.position - agent_owner.position).length_squared() >\
	 		agent_owner.max_approach_distance * agent_owner.max_approach_distance:
	
		var subgoal_move_to = preload("res://_scripts/goals/guard/goal_move_to.gd")
		subgoal = subgoal_move_to.new()
		subgoal.init(agent_owner, exit.position, true)
		subgoals.push_front(subgoal)
