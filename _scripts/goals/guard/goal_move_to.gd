# Steer guard towards a coordinate
# Keep track of Estimated Time of Arrival to judge completion and failure
extends "res://_scripts/templates/goal.gd"

var target : Vector2
var is_last_target := false
var acceptable_distance : float

var time_expected : float
var start_time : float
var is_stuck := false

func init(agent, target, is_last_target, acceptable_distance = null):
	agent_owner = agent
	self.target = target
	self.is_last_target = is_last_target
	
	# Either set acceptable distance or goal gets from the guard
	if acceptable_distance == null:
		self.acceptable_distance = agent_owner.max_approach_distance
	else :
		self.acceptable_distance = acceptable_distance
	
func activate():
	
	current_status = GOAL_STATUS.ACTIVE
	
	# Calculate ETA
	start_time = OS.get_ticks_msec()
	time_expected = start_time + ((target - agent_owner.position).length() / agent_owner.max_speed) * 1000
	# Add 2sec margin error
	time_expected += 2000.0
	agent_owner.get_steering().target_position = target
	
	# Depending if this is the last target coordinate, change behaviour
	if is_last_target:
		agent_owner.get_steering().arrive_on = true
	else :
		agent_owner.get_steering().seek_on = true
	return
	
func process() -> int:
	activate_if_inactive()
	
	agent_owner.set_heading_with_velocity()
	
	# Judge if guard become stuck/didint arrive in ETA
	if OS.get_ticks_msec() > time_expected:
		is_stuck = true
		current_status = GOAL_STATUS.FAILED
	# Judge if target arrived in destination
	elif (target - agent_owner.position).length() < acceptable_distance:
		current_status = GOAL_STATUS.COMPLETED
	
	return current_status
	
func terminate():
	agent_owner.get_steering().seek_on = false
	agent_owner.get_steering().arrive_on = false
	return
