# Moves the guard towards a formation point and keeps the formation until 
# target is in attack range
extends "res://_scripts/templates/goal.gd"

var target
var formation_point
var current_path

func init(agent, formation_point, target):
	agent_owner = agent
	self.formation_point = formation_point
	self.target = target

func activate():
	current_status = GOAL_STATUS.ACTIVE
	
	# Set movement parameters
	agent_owner.set_run()
	agent_owner.max_approach_distance = agent_owner.offensive_approach_distance

	calculate_current_path()
	populate_subgoals()
	return
	
func process() -> int:
	activate_if_inactive()
	
	# Check distance between guard and target, if they are in attack range, 
	# then terminate all subgoals and perform attack
	var dist_sqr = (agent_owner.position - target.position).length_squared()
	
	if dist_sqr <=  agent_owner.attack_range * agent_owner.attack_range:
		remove_all_subgoals()
		populate_attack()
		
	current_status = process_subgoals()
	
	# If fails, just recalculate the path and restart all subgoals
	if is_failed():
		remove_all_subgoals()
		activate()
		current_status = GOAL_STATUS.ACTIVE
		
	# If succeeded, just restart goal
	if is_completed():
		activate()
	
		
	return current_status
	
func terminate():
	remove_all_subgoals()
	return

func calculate_current_path():
	current_path = Array(agent_owner.calculate_path(formation_point.global_position))
	
func populate_subgoals():
	var subgoal_attack = preload("res://_scripts/goals/guard/goal_attack.gd")
	var subgoal_keep_formation = preload("res://_scripts/goals/guard/goal_keep_formation.gd")
	var subgoal_follow_path = preload("res://_scripts/goals/guard/goal_follow_path.gd")
	var subgoal
	
	subgoal = subgoal_attack.new()
	subgoal.init(agent_owner)
	subgoals.push_front(subgoal)
	
	subgoal = subgoal_keep_formation.new()
	subgoal.init(agent_owner, formation_point, target)
	subgoals.push_front(subgoal)
	
	subgoal = subgoal_follow_path.new()
	subgoal.init(agent_owner, current_path, target)
	subgoals.push_front(subgoal)

func populate_attack():
	var subgoal_attack = preload("res://_scripts/goals/guard/goal_attack.gd")
	var subgoal
	
	subgoal = subgoal_attack.new()
	subgoal.init(agent_owner)
	subgoals.push_front(subgoal)
