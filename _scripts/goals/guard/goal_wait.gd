# Doesnt perform any action for a duration, can be used to change guard heading
extends "res://_scripts/templates/goal.gd"

var duration : float
var target

var start_time : float
var end_time : float

func init(agent, duration : float, target = null):
	agent_owner = agent
	self.duration = duration
	self.target = target

func activate():
	current_status = GOAL_STATUS.ACTIVE
	
	# If determined, set guard heading
	if target != null:
		agent_owner.look_at(target.position)
	
	# Set timestamps
	start_time = OS.get_ticks_msec()
	end_time = start_time + duration
	
	# Disable movement
	agent_owner.get_steering().arrive_on = false
	agent_owner.get_steering().seek_on = false

	return
	
func process() -> int:
	activate_if_inactive()
	
	# Finish Goal when end timestamp is reached
	if OS.get_ticks_msec() > end_time:
		current_status = GOAL_STATUS.COMPLETED
	
	return current_status
	
func terminate():
	return
