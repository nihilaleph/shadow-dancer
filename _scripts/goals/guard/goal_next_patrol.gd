# Get a random patrol point from the current point and moves towards it
extends "res://_scripts/templates/goal.gd"

var current_target
var current_path

func init(agent):
	agent_owner = agent

func activate():
	current_status = GOAL_STATUS.ACTIVE
	
	# Set movement parameters
	agent_owner.max_speed = agent_owner.walk_speed
	agent_owner.max_wait_time = agent_owner.patrol_wait_time
	agent_owner.max_approach_distance = agent_owner.patrol_approach_distance

	# Get random available patrol point
	current_target = agent_owner.get_next_patrol_point()
	# If there isnt any, just wait for some time and try again
	if current_target == null:
		populate_wait_subgoals()
	else:
		calculate_current_path()
		populate_subgoals()
	return
	
func process() -> int:
	activate_if_inactive()
	
	current_status = process_subgoals()
	
	# If fails, just recalculate the path and restart all subgoals
	if is_failed():
		remove_all_subgoals()
		calculate_current_path()
		populate_subgoals()
		current_status = GOAL_STATUS.ACTIVE
		
	# If succeeded, just restart goal
	if is_completed():
		activate()
	
		
	return current_status
	
func terminate():
	remove_all_subgoals()
	return

func calculate_current_path():
	current_path = Array(agent_owner.calculate_path(current_target.position))
	
func populate_subgoals():
	var subgoal_wait = preload("res://_scripts/goals/guard/goal_wait.gd")
	var subgoal_follow_path = preload("res://_scripts/goals/guard/goal_follow_path.gd")
	var subgoal
	
	# Adds subgoals to look towards previous patrol point and ahead
	subgoal = subgoal_wait.new()
	subgoal.init(agent_owner, agent_owner.max_wait_time, agent_owner.previous_patrol_point)
	subgoals.push_front(subgoal)
	
	subgoal = subgoal_wait.new()
	subgoal.init(agent_owner, agent_owner.max_wait_time / 3.0, agent_owner.next_patrol_point)
	subgoals.push_front(subgoal)
	
	subgoal = subgoal_follow_path.new()
	subgoal.init(agent_owner, current_path)
	subgoals.push_front(subgoal)

	subgoal = subgoal_wait.new()
	subgoal.init(agent_owner, agent_owner.max_wait_time / 3.0, agent_owner.next_patrol_point)
	subgoals.push_front(subgoal)

func populate_wait_subgoals():
	var subgoal_wait = preload("res://_scripts/goals/guard/goal_wait.gd")
	var subgoal

	subgoal = subgoal_wait.new()
	subgoal.init(agent_owner,  agent_owner.max_wait_time)
	subgoals.push_front(subgoal)
	
