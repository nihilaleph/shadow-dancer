# Keep guard to a formation point while trying to approach target, depending
# On their distance and facing directions
extends "res://_scripts/templates/goal.gd"


var formation_point : Node2D
var target : Node2D

func init(agent, formation_point, target):
	agent_owner = agent
	self.formation_point = formation_point
	self.target = target
	
func activate():
	
	current_status = GOAL_STATUS.ACTIVE
	
	# Set movement parameters
	agent_owner.get_steering().target_object = target
	
	agent_owner.get_steering().offset_persuit_on = true
	agent_owner.get_steering().offset = formation_point.position
	return
	
func process() -> int:
	activate_if_inactive()
	
	# Keep guard looking at target
	agent_owner.look_at(target.position)

	# Use fuzzy module to arbitrate on the movement
	var danger = agent_owner.arbitrate_danger()
	
	# Guard doesnt feel threaten, walks towards target
	if danger < 33.0 :
		agent_owner.get_steering().arrive_on = true
		agent_owner.get_steering().target_position = target.position
		agent_owner.get_steering().offset_persuit_on = false
		
		agent_owner.set_slow_approach()
		
	# Guard doesnt feel too threaten, approaches slowly towards target
	elif danger < 66.0 :
		agent_owner.get_steering().arrive_on = true
		agent_owner.get_steering().offset_persuit_on = true
		agent_owner.get_steering().target_position = target.position
		
		agent_owner.set_slow_approach()
		
	# Guard feels too threaten, return to formation point
	else:
		agent_owner.get_steering().arrive_on = false
		agent_owner.get_steering().offset_persuit_on = true
		agent_owner.set_run()
		
	# If target is too far, formation was broken
	var dist_sqr = (agent_owner.position - target.position).length_squared()
		
	if dist_sqr >= formation_point.position.length_squared():
		current_status = GOAL_STATUS.FAILED
		
	
	return current_status
	
func terminate():
	agent_owner.get_steering().offset_persuit_on = false
	agent_owner.get_steering().arrive_on = false
	return
