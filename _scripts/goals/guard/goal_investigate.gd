# Moves the guard through a path of graph nodes and watch over connected
# nodes in each point
extends "res://_scripts/templates/goal.gd"

var investigate_path
var current_path
var current_target

func init(agent, path):
	agent_owner = agent
	investigate_path = path.duplicate()
	
func activate():
	current_status = GOAL_STATUS.ACTIVE
	
	# Set up movement parameters for agent
	agent_owner.set_run()
	agent_owner.max_wait_time = agent_owner.investigate_wait_time
	agent_owner.max_approach_distance = agent_owner.investigate_approach_distance

	# Gets the next node target of the path and calculate subgoals
	current_target = investigate_path.pop_front()
	calculate_current_path()
	populate_subgoals()
	
	return
	
func process() -> int:
	activate_if_inactive()
	
	current_status = process_subgoals()
	
	# If fails, just recalculate the path and restart all subgoals
	if is_failed():
		remove_all_subgoals()
		calculate_current_path()
		populate_subgoals()
		current_status = GOAL_STATUS.ACTIVE
		
	# If goal is completed but still has more graph nodes to investigate,
	# restart goal
	if is_completed() && !investigate_path.empty():
		activate()
		
	return current_status
	
func terminate():
	remove_all_subgoals()
	return
	
func calculate_current_path():
	current_path = Array(agent_owner.calculate_path_to_node(current_target))
	
func populate_subgoals():
	var subgoal_wait = preload("res://_scripts/goals/guard/goal_wait.gd")
	var subgoal_follow_path = preload("res://_scripts/goals/guard/goal_follow_path.gd")
	var next_points = agent_owner.get_connected_points(current_target)
	var subgoal
	
	# Subgoals to watch over all connected graph nodes to current target
	if investigate_path.size() > 1:
		for i in range(next_points.size()):
			subgoal = subgoal_wait.new()
			subgoal.init(agent_owner, agent_owner.max_wait_time, next_points[i])
			subgoals.push_front(subgoal)
		
	
	subgoal = subgoal_wait.new()
	subgoal.init(agent_owner, agent_owner.max_wait_time)
	subgoals.push_front(subgoal)
	
	subgoal = subgoal_follow_path.new()
	subgoal.init(agent_owner, current_path)
	subgoals.push_front(subgoal)

	#subgoal = subgoal_wait.new()
	#subgoal.init(agent_owner, agent_owner.max_wait_time / 3.0, agent_owner.next_patrol_point)
	#subgoals.push_front(subgoal)
