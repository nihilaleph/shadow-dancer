# Highest level goal. Change the subgoal as squad formation changes
extends "res://_scripts/templates/goal.gd"

func init(agent):
	agent_owner = agent
	
func activate():
	current_status = GOAL_STATUS.ACTIVE
	match agent_owner.get_squad_status():
		SquadEnum.SQUAD_STATUS.PATROL:
			set_patrol()
		_:
			populate_wait_subgoals()
	return
	
func process() -> int:
	activate_if_inactive()
	current_status = process_subgoals()
	
		
	return current_status
	
func terminate():
	remove_all_subgoals()
	return
	
# Set main goal as Patrol
func set_patrol():
	remove_all_subgoals()
	
	var subgoal = preload("res://_scripts/goals/guard/goal_next_patrol.gd").new()
	subgoal.init(agent_owner)
	subgoals.push_front(subgoal)
	
	current_status = GOAL_STATUS.ACTIVE

# Set main goal as Investigate
func set_investigate(path):
	remove_all_subgoals()
	
	var subgoal = preload("res://_scripts/goals/guard/goal_investigate.gd").new()
	subgoal.init(agent_owner, path)
	subgoals.push_front(subgoal)
	
	current_status = GOAL_STATUS.ACTIVE
	
# Set main goal as Retreat
func retreat(exit):
	remove_all_subgoals()
	
	
	var subgoal_wait = preload("res://_scripts/goals/guard/goal_wait.gd")
	var subgoal_retreat = preload("res://_scripts/goals/guard/goal_retreat.gd")
	var subgoal

	subgoal = subgoal_wait.new()
	subgoal.init(agent_owner,  agent_owner.max_wait_time, agent_owner.get_next_patrol_point(exit.id))
	subgoals.push_front(subgoal)
	
	subgoal = subgoal_retreat.new()
	subgoal.init(agent_owner, exit)
	subgoals.push_front(subgoal)
	
	current_status = GOAL_STATUS.ACTIVE
	
# Set main goal as Guard Exit
func guard_exit(exit):
	remove_all_subgoals()
	
	var subgoal = preload("res://_scripts/goals/guard/goal_guard_exit.gd").new()
	subgoal.init(agent_owner, exit)
	subgoals.push_front(subgoal)
	
	current_status = GOAL_STATUS.ACTIVE
	
# Set main goal as Offensive
func set_offensive(formation_position, target):
	remove_all_subgoals()
	
	var subgoal = preload("res://_scripts/goals/guard/goal_offensive.gd").new()
	subgoal.init(agent_owner, formation_position, target)
	subgoals.push_front(subgoal)
	
	current_status = GOAL_STATUS.ACTIVE

# Just add wait subgoal
func populate_wait_subgoals():
	var subgoal_wait = preload("res://_scripts/goals/guard/goal_wait.gd")
	var subgoal

	subgoal = subgoal_wait.new()
	subgoal.init(agent_owner,  agent_owner.max_wait_time)
	subgoals.push_front(subgoal)
	
# Push a wait subgoal, pausing any running subgoal
func push_wait_reaction(time, target):
	var subgoal_wait = preload("res://_scripts/goals/guard/goal_wait.gd")
	var subgoal

	subgoal = subgoal_wait.new()
	subgoal.init(agent_owner, time, target)
	
	if !subgoals.empty():
		subgoals[0].set_inactive()
		
	subgoals.push_front(subgoal)
	
