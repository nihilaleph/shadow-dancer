# Send a guard t o an  exit point
extends "res://_scripts/templates/goal.gd"

var current_path
var target
#onready var goal_follow_path = load("res://_scripts/goal_follow_path.gd")

func init(agent, target):
	agent_owner = agent
	self.target = target

func activate():
	current_status = GOAL_STATUS.ACTIVE
	
	# Set movement parameters
	agent_owner.max_speed = agent_owner.run_speed
	agent_owner.max_approach_distance = agent_owner.retreat_approach_distance
	
	# Avoid pushing other guards from the exit point
	agent_owner.get_steering().separation_on = false

	calculate_current_path()
	populate_subgoals()
	
	return
	
func process() -> int:
	activate_if_inactive()
	
	current_status = process_subgoals()
	
	# If fails, just recalculate the path and restart all subgoals
	if current_status == GOAL_STATUS.FAILED:
		remove_all_subgoals()
		calculate_current_path()
		populate_subgoals()
		current_status = GOAL_STATUS.ACTIVE
	
	
	return current_status
	
func terminate():
	remove_all_subgoals()
	agent_owner.get_steering().separation_on = true
	return

func calculate_current_path():
	current_path = Array(agent_owner.calculate_path(target.position))
	
func populate_subgoals():
	var subgoal_follow_path = preload("res://_scripts/goals/guard/goal_follow_path.gd")
	var subgoal
	
	subgoal = subgoal_follow_path.new()
	subgoal.init(agent_owner, current_path)
	subgoals.push_front(subgoal)
