# Givin a path, the agent will follow it
# If faced with the target enemy, interrupts movement

extends "res://_scripts/templates/goal.gd"

var goal_move_to
var current_path
# Guarantee that if the enemy is in front of him it stops following it's path
var enemy

func init(agent, path, enemy = null):
	agent_owner = agent
	current_path = path
	self.enemy = enemy

func activate():
	current_status = GOAL_STATUS.ACTIVE
	# Create a subgoal to move to next point in path
	goal_move_to = preload("res://_scripts/goals/guard/goal_move_to.gd")
	
	if !current_path.empty():
		var target = current_path.pop_front()
		
		var new_goal = goal_move_to.new()
		new_goal.init(agent_owner, target, current_path.empty())
		
		subgoals.push_front(new_goal)
		
				
	return
	
func process() -> int:
	activate_if_inactive()
	
	current_status = process_subgoals()
	
	# If current movement was completed, but there is still more points to
	# cross, reactivate the goal
	if is_completed() && !current_path.empty():
		activate()
		
	# Check if the targeted enemy is in the way of the path
	if enemy != null:
		var target_dist_sqr = (agent_owner.position - enemy.position).length_squared()
		if target_dist_sqr <  agent_owner.safe_distance * agent_owner.safe_distance && \
				agent_owner.velocity.angle_to(enemy.position - agent_owner.position) < PI / 4.0:
			current_status = GOAL_STATUS.COMPLETED
		
	return current_status
	
func terminate():
	remove_all_subgoals()
	return
