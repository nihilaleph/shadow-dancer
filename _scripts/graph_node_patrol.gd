# Extetion to graph node to include targeted flag to indicate if
# an agent already intends to advance to this node
extends "res://_scripts/templates/graph_node.gd"

var is_targeted := false
