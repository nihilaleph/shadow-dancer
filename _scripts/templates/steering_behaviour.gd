# Class implementing several steering behaviours
extends Node

# Reference to objects to be used as reference for some behaviours
var target_object : Node2D
var target_object2 : Node2D

# Coordinates to be used as reference for some behaviours
var target_position : Vector2


export var seek_on := false

export var flee_on := false

export var arrive_on := false
enum DECELERATION_LEVEL{ FAST = 1, MEDIUM = 2, SLOW = 3}
export(DECELERATION_LEVEL) var current_deceleration = 1
export var deceleration_constant := 0.3

export var intercept_on := false

export var evade_on := false

export var wander_on := false
export var wander_distance := 0.0
export var wander_radius := 0.0
export var wander_jitter := 0.0
var wander_target := Vector2(0.0, 0.0)

export var avoid_obstacle_on := false
export var obstacle_detector_length := 100.0
export var obstacle_detector_angle := 45
export var obstacle_detector_intensity := 3.0

export var interpose_on := false

export var follow_path_on := false
export var follow_path_loop := false
var path : Array
var path_index := 0
export var seek_distance := 10.0

export var offset_persuit_on := false
export var offset := Vector2(0.0, 0.0)

export var separation_on := false
export var separation_intensity := 5000.0

export var cohesion_on := false
export var cohesion_intensity := 0.5
var neighbours

# Calculate steering force for the agent according to
# which steering behaviour was turned on
func calculate() -> Vector2:
	var steering_force = Vector2(0.0, 0.0)
	var add_force = Vector2(0.0, 0.0)
	var ratio = 0.0
	
	# Calculate each steering behaviour giving order priority
	# until available force is reached
	
	if avoid_obstacle_on:
		add_force = avoid_obstacle()
		ratio = add_force_ratio(steering_force, add_force)
		steering_force += add_force * ratio
		if ratio < 1.0: 
			return steering_force
			
	if separation_on ||cohesion_on:
		neighbours = get_owner().get_node("neighbour_area").get_overlapping_bodies()
		neighbours.erase(get_owner())
	if separation_on:
		add_force = separation()
		ratio = add_force_ratio(steering_force, add_force)
		steering_force += add_force * ratio
		if ratio < 1.0: 
			return steering_force
	if cohesion_on:
		add_force = cohesion()
		ratio = add_force_ratio(steering_force, add_force)
		steering_force += add_force * ratio
		if ratio < 1.0: 
			return steering_force
	if intercept_on:
		add_force = intercept(target_object)
		ratio = add_force_ratio(steering_force, add_force)
		steering_force += add_force * ratio
		if ratio < 1.0: 
			return steering_force
	if interpose_on:
		add_force = interpose(target_object, target_object2)
		ratio = add_force_ratio(steering_force, add_force)
		steering_force += add_force * ratio
		if ratio < 1.0: 
			return steering_force
	if evade_on:
		add_force = evade(target_object)
		ratio = add_force_ratio(steering_force, add_force)
		steering_force += add_force * ratio
		if ratio < 1.0: 
			return steering_force
	if seek_on:
		add_force = seek(target_position)
		ratio = add_force_ratio(steering_force, add_force)
		steering_force += add_force * ratio
		if ratio < 1.0: 
			return steering_force
	if flee_on:
		add_force = flee(target_position)
		ratio = add_force_ratio(steering_force, add_force)
		steering_force += add_force * ratio
		if ratio < 1.0: 
			return steering_force
	if arrive_on:
		add_force = arrive(target_position, current_deceleration)
		ratio = add_force_ratio(steering_force, add_force)
		steering_force += add_force * ratio
		if ratio < 1.0: 
			return steering_force
	if follow_path_on:
		add_force = follow_path()
		ratio = add_force_ratio(steering_force, add_force)
		steering_force += add_force * ratio
		if ratio < 1.0: 
			return steering_force
			
	if offset_persuit_on:
		add_force = offset_persuit(target_object)
		ratio = add_force_ratio(steering_force, add_force)
		steering_force += add_force * ratio
		if ratio < 1.0: 
			return steering_force
	if wander_on:
		add_force = wander()
		ratio = add_force_ratio(steering_force, add_force)
		steering_force += add_force * ratio
		if ratio < 1.0: 
			return steering_force
		
	return steering_force

# Move full speed to a target
func seek(target : Vector2) -> Vector2:
	var to_target = target - get_owner().position
		
	var target_vel = (to_target).normalized() * get_owner().max_speed
	return target_vel - get_owner().velocity

# Move full speed opposite to target
func flee(target : Vector2) -> Vector2:
	var target_vel = (get_owner().position - target).normalized() * get_owner().max_speed
	return target_vel - get_owner().velocity

# Move to target with deceleration
func arrive(target: Vector2, deceleration : int) -> Vector2:
	var to_target = target - get_owner().position
	var distance = to_target.length()
	
	# Speed is proportional to distance against deceleration degree
	var speed = distance / (deceleration_constant * deceleration)
	
	speed = min(speed, get_owner().max_speed)
	var target_vel = to_target * speed / distance
	
	return target_vel - get_owner().velocity

# Move to target taking into account target velocity
func intercept(target: Node2D)-> Vector2:
	var to_target = target.position - get_owner().position
	
	# Check if target and owner already are going to meet
	if to_target.dot(target.velocity) > 0 && get_owner().velocity.dot(target.velocity) < -0.95:
		return seek(target.position)
	
	# Project min time to arrive at target current position
	var look_ahead_time = to_target.length() / (target.velocity.length() + get_owner().max_speed)
	return seek(target.position + target.velocity * look_ahead_time)
	
# Move full speed opposite to target taking into account target velocity
func evade(target: Node2D)-> Vector2:
	var to_target = target.position - get_owner().position
	
	# Project min time to arrive at target current position
	var look_ahead_time = to_target.length() / (target.velocity.length() + get_owner().max_speed)
	return flee(target.position + target.velocity * look_ahead_time)
	
# Move semi-randomly according to virtual target in front of agent
func wander() -> Vector2:
	# Create jitter for target
	wander_target += Vector2(rand_range(-1.0, 1.0) * wander_jitter, rand_range(-1.0, 1.0) * wander_jitter)
	
	# Reproject target in radius
	wander_target = wander_target.normalized() * wander_radius
	
	# Translate to distance
	var local_target = wander_target + Vector2(wander_distance, 0.0)
	
	# Project with current velocity
	var final_target = local_target.rotated(get_owner().velocity.angle())
	
	return final_target
	
# Detect obstacles using agent's obstacle_detector and avoid them
func avoid_obstacle() -> Vector2:
	var avoid_force = Vector2(0.0, 0.0)
	var speed = get_owner().velocity.length()
	if (speed > 10.0):
		var direction = get_owner().velocity / speed
		var obstacle_detector_reach = obstacle_detector_length * (1.0 + speed / get_owner().max_speed)
		
		var obstacle_detector_vector = obstacle_detector_reach * direction
		
		var obstacle_detector_front = get_owner().get_node("obstacle_detector_front")
		var obstacle_detector_left = get_owner().get_node("obstacle_detector_left")
		var obstacle_detector_right = get_owner().get_node("obstacle_detector_right")
		
		obstacle_detector_front.cast_to = obstacle_detector_vector
		obstacle_detector_left.cast_to = obstacle_detector_vector.rotated(deg2rad(obstacle_detector_angle))
		obstacle_detector_right.cast_to = obstacle_detector_vector.rotated(deg2rad(-obstacle_detector_angle))
		
		# Only consider the highest penetration of raycast
		# Initial value for comparison is the mminimum
		var raycast_penetration = -1.0
		
		if obstacle_detector_front.is_colliding():
			var contact_point = obstacle_detector_front.get_collision_point()
			var contact_normal = obstacle_detector_front.get_collision_normal()
			var penetration = (obstacle_detector_vector - (contact_point - get_owner().position)).length()
			if raycast_penetration < penetration:
				avoid_force = penetration * contact_normal
		
		if obstacle_detector_left.is_colliding():
			var contact_point = obstacle_detector_left.get_collision_point()
			var contact_normal = obstacle_detector_left.get_collision_normal()
			var penetration = (obstacle_detector_vector - (contact_point - get_owner().position)).length()
			if raycast_penetration < penetration:
				avoid_force = penetration * contact_normal
				
		if obstacle_detector_right.is_colliding():
			var contact_point = obstacle_detector_right.get_collision_point()
			var contact_normal = obstacle_detector_right.get_collision_normal()
			var penetration = (obstacle_detector_vector - (contact_point - get_owner().position)).length()
			if raycast_penetration < penetration:
				avoid_force = penetration * contact_normal
				
		avoid_force *= obstacle_detector_intensity
		
	return avoid_force

# Arrive to midpoint of two targets taking into account targets velocity
func interpose(target: Node2D, target2: Node2D) -> Vector2:
	var mid_point = (target.position + target2.position) / 2
	# Project min time to arrive at current midpoint
	var look_ahead_time = (mid_point - get_owner().position).length() / get_owner().max_speed
	
	# New midpoint after look ahead time
	mid_point = ((target.position + target.velocity * look_ahead_time) + (target2.position + target2.velocity * look_ahead_time)) / 2
	
	return arrive(mid_point, DECELERATION_LEVEL.FAST)

# Move through an array of points
func follow_path() -> Vector2:
	var to_target = path[path_index] - get_owner().position
	var distance_squared = to_target.length_squared()
	var path_size = path.size()
	
	if distance_squared < seek_distance * seek_distance:
		if path_index >= path_size - 1:
			if follow_path_loop:
				path_index = 0
		else:
			path_index += 1
		
	if path_index < path_size - 1:
		return seek(path[path_index])
	elif path_index == path_size - 1:
		if follow_path_loop:
			return seek(path[path_index])
		else:
			return arrive(path[path_index], DECELERATION_LEVEL.MEDIUM)
	
	return Vector2(0.0, 0.0)

# Move to target with an offset and taking into account velocity
func offset_persuit(target: Node2D) -> Vector2:
	var rotated_offset = offset.rotated(target.velocity.angle())
	var target_offset = target.position + rotated_offset
	var to_target = target_offset - get_owner().position
	
	# Project min time to arrive at target current position
	var look_ahead_time = to_target.length() / (target.velocity.length() + get_owner().max_speed)
	
	
	return arrive(target_offset + target.velocity * look_ahead_time, DECELERATION_LEVEL.FAST)

# Try to keep away from neighbours
func separation() -> Vector2:
	var separation_force = Vector2(0.0, 0.0)
	for neighbour in neighbours:
		var to_neighbour = get_owner().position - neighbour.position
		# Separation force is oposite to neighbour and inversely proportional to the distance
		separation_force += to_neighbour / to_neighbour.length_squared()
	return separation_force * separation_intensity

# Try to stay together with neighbours
func cohesion() -> Vector2:
	var center_of_mass = Vector2(0.0, 0.0)
	var neighbour_count = neighbours.size()
	if neighbour_count > 0:
		for neighbour in neighbours:
			center_of_mass += neighbour.position
		
		center_of_mass /= neighbour_count
	
		return seek(center_of_mass) * cohesion_intensity
	
	return Vector2(0.0, 0.0)

# Returns the percentage of the add_force that can be added in accumulated_force
func add_force_ratio(accumulated_force : Vector2, add_force : Vector2) -> float:
	var force_left = get_owner().max_force - accumulated_force.length()
	var add_force_intensity = add_force.length()
	if add_force_intensity > force_left:
		return force_left / add_force_intensity
	else :
		return 1.0
