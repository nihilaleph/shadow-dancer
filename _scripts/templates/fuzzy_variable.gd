# Performs calculation on children fuzzy sets
extends Node

export var min_range := 0.0
export var max_range := 0.0

# Clamp value to min and max range and calculate Degree of Membership to 
# each fuzzy set that belongs to this variable
func fuzzify(value : float):
	value = clamp(value, min_range, max_range)
	for fuzzy_set in $fuzzy_sets.get_children():
		fuzzy_set.calculate_dom(value)
	pass
	
# Defuziffy variable using max average method
func defuzzify_max_average() -> float:
	var final_value := 0.0
	var total_confidence := 0.0
	
	# Multiply the Degree Of Membership with the Representative Value of the set
	for fuzzy_set in $fuzzy_sets.get_children():
		final_value += fuzzy_set.get_dom() * fuzzy_set.get_representative_value()
		total_confidence += fuzzy_set.get_dom()
	# Avoid division by 0
	if total_confidence == 0.0:
		return 0.0
		
	return final_value / total_confidence
	
# Defuziffy variable using centroid method
##### To be implemented
func defuzzify_centroid(num_samples : int) -> float:
	return 0.0
