extends Node

# Calculate Degree of Membership to a specific fuzzy variable according 
# to determined value
func fuzzify(fuzzy_variable, value):
	fuzzy_variable.fuzzify(value)
	pass

# Defuziffy a determined fuzzy variable according to the set of rules
# of the module
func defuzzify(fuzzy_variable):
	var rules = $rules.get_children()
	for rule in rules:
		rule.clear_consequence()
	
	for rule in rules:
		rule.calculate()
		
	return fuzzy_variable.defuzzify_max_average()
