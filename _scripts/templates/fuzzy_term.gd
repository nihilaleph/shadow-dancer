# This is a virtual class to serve as template for all different Fuzzy Terms
# It needs to return a Degree of Membership of the corresponding term,
# an operation to clear any saved DoM, and be able to perform OR with
# another value
extends Node

# Return result of operation
func get_dom() -> float:
	return 0.0
	
# Erase any DOM saved in this term
func clear_dom():
	pass
	
# Operate OR on this term
func or_with_dom(value : float):
	pass
