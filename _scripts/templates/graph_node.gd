# Represents an Euclidian node in a graph
# Retains an id and visited tag
# Id is usually set as the index in the array it belongs to
extends Position2D

var id : int
var visited := false
