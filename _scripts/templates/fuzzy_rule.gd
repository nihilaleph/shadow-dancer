# Used to calculate Degree of Membership value from an antecedent term
# to a consequent term
extends Node

# Clear the consequence value from any DoM already calculated
func clear_consequence():
	$consequence.clear_dom()

# Calculates the antecedent and apply result in the consequence
func calculate():
	$consequence.or_with_dom($antecedent.get_dom())
