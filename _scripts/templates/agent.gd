extends KinematicBody2D

# Movement and direction
var velocity : Vector2
var heading := Vector2(1.0, 0.0)

# Physics parameters
export var mass := 1.0
export var max_speed := 100.0
export var max_force := 100.0
export var deceleration := .9

# Perception parameters
export var reaction_time := .5
var reaction_cooldown := 0.0

func _physics_process(delta):
	$heading_vector.cast_to = heading * 50.0
	
	########## Agent movement
	# Calculate movement from steering behavouir
	var acceleration = $steering_behaviour.calculate() / mass
	velocity += acceleration * delta
	
	velocity = velocity.clamped(max_speed)
	
	move_and_slide(velocity)
	
	# Decelerate agent if there is no movement to be done
	if acceleration.length_squared() == 0:
		velocity *= deceleration
		
	######### Agent perception
	# Update perception after reaction time
	reaction_cooldown += delta
	if reaction_cooldown > reaction_time:
		process_perception()
		reaction_cooldown = 0

func get_steering() : 
	return $steering_behaviour

func process_perception():
	pass
		
