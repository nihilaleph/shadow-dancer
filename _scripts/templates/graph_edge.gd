# Represents an Edge on a graph
tool
extends CanvasItem

# References from and to nodes of a graph
export(NodePath) var from_path
var from

export(NodePath) var to_path
var to

# Set edge as bidirectional
export var is_bidirectional := true

func _ready():
	from = get_node(from_path)
	to = get_node(to_path)

# Draws a line between the nodes the edge connects
# Still don't update automatically whenever a node changes position
func _draw():
	if not Engine.editor_hint || !is_instance_valid(from) || !is_instance_valid(to):
		pass
	#	return
	
	draw_line(from.position, to.position, Color.antiquewhite)
	
