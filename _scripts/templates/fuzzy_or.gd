# Fuzzy Operation OR
extends "res://_scripts/templates/fuzzy_term.gd"

# References to the terms it is going to operate on
export(NodePath) var term1
export(NodePath) var term2

func get_dom() -> float:
	return max(get_node(term1).get_dom(), get_node(term2).get_dom())
	
func clear_dom():
	get_node(term1).clear_dom()
	get_node(term2).clear_dom()
	
func or_with_dom(value : float):
	get_node(term1).or_with_dom(value)
	get_node(term2).or_with_dom(value)
