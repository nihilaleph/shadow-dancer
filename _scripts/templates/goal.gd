# Virtual class for Goal
extends Node

enum GOAL_STATUS { INACTIVE, ACTIVE, COMPLETED, FAILED }

# Current Goal Status
var current_status := 0
# Reference to agent who owns this goal
var agent_owner

# LIFO pile of subgoals
var subgoals : Array

func _ready():
	current_status = GOAL_STATUS.INACTIVE
	
# Since goals might have different signature for init, and Godot does not 
# allow function override, this is commented out, but all goals should 
# initialize with at least the reference of agent owner
#func init(agent):
#	agent_owner = agent

# Whenever a Goal is INACTIVE, this function should be fired to start process
func activate():
	return
	
# Function that is always called to achieve a goal
func process() -> int:
	return GOAL_STATUS.INACTIVE
	
# Executed before ceasing to exist
func terminate():
	return
	
func is_inactive() -> bool:
	return current_status == GOAL_STATUS.INACTIVE
	
func is_active() -> bool:
	return current_status == GOAL_STATUS.ACTIVE
	
func is_completed() -> bool:
	return current_status == GOAL_STATUS.COMPLETED
	
func is_failed() -> bool:
	return current_status == GOAL_STATUS.FAILED

# Runs through the subgoal list, liberating any finished goal and
# processing the first in the pile
func process_subgoals() -> int:
	# Eliminate all subgoals already finished on the LIFO
	while !subgoals.empty() && (subgoals[0].is_completed() || subgoals[0].is_failed()):
		var goal = subgoals.pop_front()
		goal.terminate()
		goal.queue_free()
		
	# Considered completed if subgoals are completed
	if subgoals.empty():
		return GOAL_STATUS.COMPLETED
		
	# Process only the first subgoal in LIFO
	else:
		var status = subgoals[0].process()
		
		# Even if completed, if there are still more subgoals to goal, this is still active
		if status == GOAL_STATUS.COMPLETED && subgoals.size() > 1:
			return GOAL_STATUS.ACTIVE
			
		# This goal reflect status of current subgoal
		return status

# Terminate and clear every subgoals in the pile
func remove_all_subgoals():
	for subgoal in subgoals:
		subgoal.terminate()
		subgoal.queue_free()
		
	subgoals.clear()
	
# Set current goal and subgoals as Inactive
func set_inactive():
	current_status = GOAL_STATUS.INACTIVE
	for subgoal in subgoals:
		subgoal.set_inactive()
		
	subgoals.clear()

# Checks if inactive. If so activates it. Used as first line in process()
func activate_if_inactive():
	if is_inactive():
		activate()
