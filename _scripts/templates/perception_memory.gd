# Operates perception of Sight and Hearing, as well as retain the information
# as long as the memory span
extends Node2D

# Emit this signal whenever something was sensed
signal sensed_something(position)

# How many miliseconds the memory of a sense should last
export var memory_span := 2000

# Timestamp of thelast time something was sensed
var last_time_sensed := 0.0

# Properties of Vision

# Indicate if a target was seen in the last memory span
var has_seen_target := false
var time_became_visible := 0.0
var last_time_visible := 0.0
var is_target_in_lof := false
var last_seen_target : Node2D
var last_seen_position : Vector2
export var fov_angle := PI / 3.0

# Properties of Hearing

# Indicate if a target was heard in the last memory span
var has_heard_target := false
var time_became_audible := 0.0
var last_time_audible := 0.0
var last_heard_position : Vector2


# Used to check Vision and update it's memory
func process_perception():
	process_view()
	
	# Check if last time sensed target is over memory span
	# If so, drop that perception
	var current_time = OS.get_ticks_msec()
	if has_heard_target:
		if last_time_audible + memory_span < current_time:
			has_heard_target = false
			
	if !is_target_in_lof && has_seen_target:
		if last_time_visible + memory_span < current_time:
			has_seen_target = false

# Should be fired whenever the agent heard something
func process_sound(sound_source : Vector2):
	# Update all values in memory
	if !has_heard_target:
		has_heard_target = true
		time_became_audible = OS.get_ticks_msec()
		emit_signal("sensed_something", sound_source)
		
	last_heard_position = sound_source
	last_time_sensed = OS.get_ticks_msec()
	last_time_audible = last_time_sensed

func process_view():
	# Assume there is no target on FOV
	is_target_in_lof = false
	var found_bodies = $field_of_view.get_overlapping_bodies()
	var players = get_tree().get_nodes_in_group("players")
	var new_target = null
	# Compare overlapping bodies with players
	for player in players:
		if found_bodies.find(player) > 0:
			new_target = player
	# Stop if no target was found
	if new_target == null || !new_target.is_visible:
		return
		
	# Check if there is line of sight
	$line_of_sight.cast_to = new_target.position - get_owner().position

	if $line_of_sight.is_colliding():
		return
		
	# Check if it's inside the view angle
	if abs(get_owner().heading.angle_to(new_target.position - get_owner().position)) < fov_angle:
		# Update all values in memory
		is_target_in_lof = true
		last_seen_target = new_target
		last_seen_position = new_target.position
		if !has_seen_target:
			has_seen_target = true
			time_became_visible = OS.get_ticks_msec()
			emit_signal("sensed_something", new_target)
			
		last_time_sensed = OS.get_ticks_msec()
		last_time_visible = last_time_sensed


func get_delta_time_visible() -> float:
	return last_time_visible - time_became_visible
	
func get_delta_time_audible() -> float:
	return last_time_audible - time_became_audible
