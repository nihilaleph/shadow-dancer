# Basic operating class for a Finite State Machine

extends Node

# Keep current states on stack, in case need to return
var states_stack = []
# Global state of behaviour
var global_state
# Owner of the FSM
var agent

export(NodePath) var global_state_path
export(NodePath) var initial_state_path

# Called when the node enters the scene tree for the first time.
func _ready():
	agent = get_parent()
	
	states_stack.push_front(get_node(initial_state_path))
	global_state = get_node(global_state_path)
	pass # Replace with function body.
	
func _process(delta):
	if agent == get_tree().root:
		execute()

# Execute FSM
func execute():
	# Execute global state if there is any registered
	if is_instance_valid(global_state):
		global_state.execute(agent)
		
	states_stack[0].execute(agent)
	
# Cause FSM to change state, and stack previous state, if necessary
func change_state(new_state, stack_previuos_state := false):
	states_stack[0].exit(agent)
	
	# Take current state out of the stack if not required
	if stack_previuos_state:
		states_stack.pop_front()
		
	states_stack.push_front(new_state)
	
	states_stack[0].enter(agent)

# Reverts to a previous state.
# If there is no state saved in the stack returns false
func revert_state() -> bool:
	if states_stack.size() > 1:
		states_stack[0].exit(agent)
		states_stack.pop_front()
		states_stack[0].enter(agent)
		return true
	else :
		return false

