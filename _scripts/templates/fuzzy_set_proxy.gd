# A term that simply references a set of a fuzzy variable and call its methods
extends "res://_scripts/templates/fuzzy_term.gd"

export (NodePath) var fuzzy_set_path


func get_dom() -> float:
	return get_node(fuzzy_set_path).get_dom()
	
func or_with_dom(value : float):
	return get_node(fuzzy_set_path).or_with_dom(value)
	
func clear_dom():
	get_node(fuzzy_set_path).clear_dom()
	
func calculate_dom(value : float) -> float:
	return get_node(fuzzy_set_path).calculate_dom(value)

func get_representative_value() -> float:
	return get_node(fuzzy_set_path).get_representative_value()
