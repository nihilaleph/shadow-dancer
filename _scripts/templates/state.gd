# Virtual class for a state for Finite State Machine
extends Node

# Called whenever the FSM enters this state
func enter(agent):
	return

# Performs the actual execution of the state
func execute(agent):
	return

# Called whenever the FSM leaves the state
func exit(agent):
	return
