# Represent a fuzzy set to be part of a fuzzy variable
extends "res://_scripts/templates/fuzzy_term.gd"

# Represents the Degree of Membership that value has in this set
var degree_of_membership : float

# Fuzzy set types to determine how the calculation will be performed
enum FUZZY_SET_TYPE { LEFT_SHOULDER, TRIANGLE, RIGHT_SHOULDER }
export(FUZZY_SET_TYPE) var fuzzy_set_type

# Parameters of the set, its meaning depends on the type selected
export var peak_point := 0.0
export var left_offset := 0.0
export var right_offset := 0.0

func get_dom() -> float:
	return degree_of_membership

# Perform OR operation with a value and saves in DoM
func or_with_dom(value : float):
	degree_of_membership = max(degree_of_membership, value)
	return degree_of_membership
	
# Reset DoM to 0.0
func clear_dom():
	degree_of_membership = 0.0
	
# Calculates DoM value from given value, depending on the type of set
func calculate_dom(value : float) -> float:
	match fuzzy_set_type:
		FUZZY_SET_TYPE.LEFT_SHOULDER:
			degree_of_membership = calculate_dom_left_shoulder(value)
		FUZZY_SET_TYPE.RIGHT_SHOULDER:
			degree_of_membership = calculate_dom_right_shoulder(value)
		FUZZY_SET_TYPE.TRIANGLE:
			degree_of_membership = calculate_dom_triangle(value)
		_:
			degree_of_membership = 0.0
			
	return degree_of_membership

# Representative value of a set is used to defuzzify
# Calculation dependent of set type
func get_representative_value() -> float:
	match fuzzy_set_type:
		FUZZY_SET_TYPE.LEFT_SHOULDER:
			return get_representative_value_left_shoulder()
		FUZZY_SET_TYPE.RIGHT_SHOULDER:
			return get_representative_value_right_shoulder()
		FUZZY_SET_TYPE.TRIANGLE:
			return get_representative_value_triangle()
		_:
			return 0.0

# Calculations if set is a left shoulder
func calculate_dom_left_shoulder(value : float) -> float:
	if (left_offset <= 0.0 && value == peak_point):
		return 1.0
		
	if value <= peak_point && value >= (peak_point - left_offset):
		var grad = 1.0 / left_offset
		return grad * (value - (peak_point - left_offset))
		
	elif value > peak_point && value < (peak_point + right_offset):
		return 1.0
		
	else:
		return 0.0

func get_representative_value_left_shoulder() -> float:
	return (peak_point + (peak_point - left_offset)) / 2.0
	
	
# Calculations if set is a right shoulder
func calculate_dom_right_shoulder(value : float) -> float:
	if (left_offset <= 0.0 && value == peak_point):
		return 1.0
		
	if value <= peak_point && value >= (peak_point - left_offset):
		return 1.0
		
	elif value > peak_point && value < (peak_point + right_offset):
		var grad = 1.0 / (- right_offset)
		return grad * (value - peak_point) + 1.0
		
	else:
		return 0.0

func get_representative_value_right_shoulder() -> float:
	return (peak_point + (peak_point + right_offset)) / 2.0


# Calculations if set is a triangle
func calculate_dom_triangle(value : float) -> float:
	if (right_offset <= 0.0 && value == peak_point) || (left_offset <= 0.0 && value == peak_point):
		return 1.0
		
	if value <= peak_point && value >= (peak_point - left_offset):
		var grad = 1.0 / left_offset
		return grad * (value - (peak_point - left_offset))
		
	elif value > peak_point && value < (peak_point + right_offset):
		var grad = 1.0 / (- right_offset)
		return grad * (value - peak_point) + 1.0
		
	else:
		return 0.0

func get_representative_value_triangle() -> float:
	return peak_point
