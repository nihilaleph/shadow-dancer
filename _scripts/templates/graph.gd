# Holds graph nodes and edges
extends Node2D

# Retains a AStar instance to perform queries
onready var astar := AStar2D.new()

# Hold the list of nodes and edges of the graph
var nodes
var edges

func _ready():
	nodes = $nodes.get_children()
	edges = $edges.get_children()
	
	# Initialize AStar instance with graph properties
	for i in range(nodes.size()):
		nodes[i].id = i
		astar.add_point(i, nodes[i].position)
		
	for i in range(edges.size()):
		astar.connect_points(edges[i].from.id, edges[i].to.id, edges[i].is_bidirectional)
		
# Clear visited tag of all nodes
func clear_visited_from_nodes():
	for i in range(nodes.size()):
		nodes[i].visited = false
		
# Check visited tag for all nodes
func are_all_nodes_visited() -> bool:
	var are_visited = true
	for i in range(nodes.size()):
		are_visited = are_visited && nodes[i].visited
		
	return are_visited
