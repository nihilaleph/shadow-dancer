extends KinematicBody2D

# Movement Parameters
export var max_speed := 150
export var dash_distance := 500
var velocity := Vector2(0.0, 0.0)
var heading := Vector2(1.0, 0.0)
# Flag to indicate invisibility during dash
var is_invisible := false
# Flag to indicate visibility by torches
var is_visible := false

# Dash variables for movemoent locking
var is_in_animation := false
var can_dash = true
export var dash_duration := 1.0
var dash_vector := Vector2(0.0, 0.0)
var dash_timer := 0.0

# Hold any illumantion source that sees the player
var illumination_source : Array

# Used to transit animation
onready var animation_state = $AnimationTree.get("parameters/playback")

# Called when the node enters the scene tree for the first time.
func _ready():
	$Tween.connect("tween_completed", self, "dash_completed")
	pass # Replace with function body.

func _unhandled_input(event):
		
	match event.get_class():
		"InputEventKey":
			# Movement input
			var vertical = Input.get_action_strength("player_down") - Input.get_action_strength("player_up")
			var horizontal = Input.get_action_strength("player_right") - Input.get_action_strength("player_left")
			
			if vertical == 0.0 && horizontal == 0.0:
				velocity = Vector2(0.0,0.0)
			else :
				heading = Vector2(horizontal, vertical).normalized()
				$RayCast2D.cast_to = heading * 50
				velocity = heading * max_speed
			
			# Dash input
			if Input.is_action_just_pressed("player_dash") && can_dash:
				
				animation_state.travel("dash")
				$AnimationTree.set("parameters/dash/blend_position", heading)
				velocity = Vector2(0.0,0.0)
				can_dash = false
				is_invisible = true
				is_in_animation = true
				set_collision_layer_bit(0, false) 
				set_collision_mask_bit(0, false) 
				dash_vector = heading * dash_distance
				
			if Input.is_action_just_pressed("player_restart"):
				get_tree().reload_current_scene()

func _physics_process(delta):
	# Update animation parameters
	$AnimationTree.set("parameters/idle/blend_position", heading)
	$AnimationTree.set("parameters/walk/blend_position", heading)
	# Perform dash animation only if it is dashing
	if is_in_animation:
		move_and_slide(dash_vector)
		dash_timer += delta
		if dash_timer > dash_duration:
			dash_completed()
	# Else animate according to velocity
	else:
		if velocity.x == 0 && velocity.y == 0:
			animation_state.travel("idle")
		else:
			animation_state.travel("walk")
		move_and_slide(velocity)

# Clear all dash related properties
func dash_completed():
	is_invisible = false
	is_in_animation = false
	set_collision_layer_bit(0, true) 
	set_collision_mask_bit(0, true) 
	dash_timer = 0.0
	
	can_dash = illumination_source.empty()
	pass

# Called whenever the player enters or leaves a torch Line of Sight
func illuminate(source : Node, illuminated : bool):
	if illuminated:
		if illumination_source.find(source) < 0:
			illumination_source.push_front(source)
			
	else:
		illumination_source.erase(source)
		
	is_visible = !illumination_source.empty()
	can_dash = can_dash || !is_visible
