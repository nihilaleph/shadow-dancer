# Shadow Dancer
This is a small project created to practice different game AI techniques. It was developed on [Godot Engine 3.2.1](https://godotengine.org/) and the book [Programming Game AI by Example, by Mat Buckland](https://www.amazon.com/Programming-Example-Wordware-Developers-Library-ebook/dp/B0029LCJXE) was the main reference for it. Though this is not a thorough documentation, the following description will hopefully help to understand how the different concepts were implemented.

[[_TOC_]]

## Finite State Machine

```
.
+-- _scenes
| 	+-- templates
|	|	+-- fsm.tscn
|   +-- fsm_squad.tscn
|   +-- garden.tscn
+-- _scripts
| 	+-- templates
|	|	+-- fsm.gd
|	|	+-- state.gd
| 	+-- states
|	|	+-- guard
|	|	|	+-- state_defensive.gd
|	|	|	+-- state_offensive.gd
|	|	|	+-- state_patrol.gd
|	|	|	+-- state_retreat.gd
|	|	|	+-- state_warning.gd
|	+-- squad.gd
```
### Template
The FSM template scene is composed by a Node attached to the *_scripts/templates/fsm.gd* script, and a child Node that carries all the states possible in the FSM. Each state is another Node, that will have attached a script that extends the _scripts/templates/state.gd script.

*_scripts/templates/fsm.gd*
This script implements the basic operations of a FSM, having a **stack of states** to execute, a **global state**, operations to **execute**, **change state** and **revert state**

*_scripts/templates/state.gd*
This script simply outlines the necessary functions to work with the FSM, being them **enter** and **exit**, which are called during fsm.change_state and fsm.revert_state, and **execute**, which is called from the fsm.execute

### In Shadow Dancer
The FSM is utilized by the squad, which controls the high level strategy of the guards in the garden scenario.
```mermaid
graph LR
A(Patrol) -- Something sensed --> B(Warning)
B -- Time out --> A
B -- Intruder identified --> C(Offensive)
C -- Lost sight --> D(Retreat)
A -- Intruder identified --> C
D -- Intruder identified --> C
D -- Exit secured --> E(Defensive)
E -- Intruder identified --> C
```
*Patrol State*
Guards keep patrolling randomly through the map

*Warning State*
Closest guards to exit secure it, while the others keep patrolling

*Offensive State*
Guards try to enclose intruder according to a formation, and when they are in range, attack

*Retreat State*
Divide and send the guards to secure the exits

*Defensive State*
Once the exits are secured, pick guards to investigate a fixed path through the map. Once the investigation is completed, rotate the guards

## Goals & Subgoals
```
.
+-- _scenes
|   +-- guard.tscn
+-- _scripts
| 	+-- templates
|	|	+-- goal.gd
| 	+-- goals
|	|	+-- guard
|	|	|	+-- goal_attack.gd
|	|	|	+-- goal_follow_path.gd
|	|	|	+-- goal_guard_exit.gd
|	|	|	+-- goal_investigate.gd
|	|	|	+-- goal_keep_formation.gd
|	|	|	+-- goal_move_to.gd
|	|	|	+-- goal_next_patrol.gd
|	|	|	+-- goal_offensive.gd
|	|	|	+-- goal_retreat.gd
|	|	|	+-- goal_thinker.gd
|	|	|	+-- goal_wait.gd
|	+-- guard.gd
```
### Template
The goals are implemented purely as a class, not having a scene or node attached to them, since they are generated and discarded dynamically. They are initiated as **Inactive**, turned **Active** when processing, and resulted in a **Completed** of **Failed** state, depending on their own criteria. They are also capable of nesting subgoals, which will be processed one at a time until completion or failure. The subgoals are stacked in a pile, and only the first one is proceesed in a process cycle.

*_scripts/templates/goal.gd*
This virtual class is used to outline the basic functions of a goal class, such as **activate**, **process**, **terminate**, **process subgoals**, **remove all subgoals**, **set inactive**, and **activate if inactive**.

### In Shadow Dancer

#### Atomic goals
Atomic goals are the goals that does not have any subgoals and perform a singular task for the agent. In this implementation of a guard, the following were implemented:

| Atomic Goal     | Action |
| -------------   | ------- |
| Attack         | Perform an attack |
| Keep formation | Stay in formation position and try to approach a target using [fuzzy logic](#fuzzy-logic) |
| Move to         | Move to a target with ETA
| Wait           | Stop movement and face to a direction |

#### Composite goals
Composite goals are goals that are made of other subgoals, them being either atomic goals or other composite goals. To better visualize the capabilities of a guard, a top-down visualization of the goals hierarchy will be presented.

The **Goal Thinker** is the top-most goal for a guard. Through it, the guard can decide to set any high level goal, as well as push a Wait Goal, in case they are surprised by something they sensed. In the current implementation, the guard high level goals are mainly decided by the squad state.
```mermaid
graph TB
A((Thinker)) --> B((Next Patrol))
A --> C((Investigate))
A --> D((Retreat))
A --> E((Guard Exit))
A --> F((Offensive))
A --> G((Wait))
```
*Next Patrol*
Responsible to select a random node from the [map graph](#graph) and make the guard move towards it, and look around. When it is completed, it restarts the process.

```mermaid
graph LR
A((Next Patrol))--> B
subgraph Subgoals
B[Wait] --> C((Follow Path))
C --> D[Wait]
D -. Completed .-> B
end
```
*Follow Path*
Responsible to create a path from a target point and move through it. For each point in the path, a **Move To** goal is created. If there is a enemy target the guard is searching for, and it is in a middle point of the path, the goal is also considered completed.

```mermaid
graph LR
A((Follow Path))--> B
subgraph Subgoals
B((Move To)) -- ... --> B
end
```

*Investigate*
Responsible to follow a specific path from the [map graph](#graph) and make the guard move through it, while looking around.

```mermaid
graph LR
A((Investigate))--> B
subgraph Subgoals
B((Follow Path)) --> C[Wait]
C --> D[Wait]
D -. Completed .-> B
end
```
*Retreat*
Responsible to follow a specific path from the [map graph](#graph) and make the guard move through it, while looking around.

```mermaid
graph LR
A((Retreat))--> B
subgraph Subgoals
B((Follow Path))
end
```
*Guard Exit*
Responsible to keep near an exit point and look around for intruders.

```mermaid
graph LR
A((Guard Exit))--> B
subgraph Subgoals
B[Move To] --> C[Wait]
C --> D[Wait]
D --> E[Wait]
E -. Completed .-> B
end
```
*Offensive*
Responsible to move the guard to a selected formation point determined by the squad, keep that formation while trying to approach the target, and attack when in range.

```mermaid
graph LR
A((Offensive))--> B
subgraph Subgoals
B((Follow Path)) --> C[Keep Formation]
C --> D[Attack]
D -. Completed .-> B
end
```
## Steering Behaviour

```
.
+-- _scenes
| 	+-- templates
|	|	+-- agent.tscn
|   +-- guard.tscn
+-- _scripts
| 	+-- templates
|	|	+-- steering_vehaviour.gd
```

### Template
The steering behaviour is what determines the agent movement behaviour. Such behaviours (and parameters) include:
* **Seek** (target position)
* **Flee** (target position)
* **Arrive** (target position, deceleration level)
* **Intercept** (target object)
* **Evade** (target object)
* **Wander** (target distance, radius, jitter)
* **Avoid Obstacle** (detector length, angle, intensity)
* **Interpose** (target object1, target object 2)
* **Follow Path** (path, is loop, seek distance)
* **Offset Pursuit** (target object, offset)
* **Separation** (intensity)
* **Cohesion** (intensity)

Each behaviour has a priority to be actuated, since the steering force of the agent is considered finite, and can be turned on and off as necessary.

### In Shadow Dancer
The agents turn on and off the steering behaviours according to their goals. **Seek**, **Arrive**, **Avoid Obstacle**, **Offset Pursuit** and **Separation** are the ones mainly used.
## Graph
```
.
+-- _scenes
| 	+-- templates
|	|	+-- graph.tscn
|	|	+-- graph_edge.tscn
|	|	+-- graph_node.tscn
|   +-- graph_garden.tscn
|   +-- graph_node_patrol.tscn
|   +-- garden.tscn
+-- _scripts
| 	+-- templates
|	|	+-- graph.gd
|	|	+-- graph_edge.gd
|	|	+-- graph_node.gd
|	+-- graph_node_patrol.gd
```
### Template
A graph is used in this project to function as a map to points of interest in the scene, namely, points of patrol. The graph template scene is composed by a node attached to the graph template script, as well as two node children that hold array of graph nodes and graph edges.

*_scripts/templates/graph.gd*
Godot has already a AStar2D class to perform AStar graph searches, so an instance is created and populated with those arrays. This node then is used as an interface to the class.

*_scripts/templates/graph_node.gd*
A graph node is merely a position in 2D, with a boolean to flag if this node has been visited already, and an id to identify it in the AStar class.

*_scripts/templates/graph_edge.gd*
This class represents the connection between two nodes, being it bidirectional or not. It also has the functionality to draw a line between the nodes in the level editor.

### In Shadow Dancer
The graph for the garden level represent all the patrol points that are to be used by the guards and squad. It also classify some nodes as the Exit nodes, to be used in some of the behaviours of the squad.

*_scripts/graph_node_patrol.gd*
This extension of the graph node class simply adds the flag **is_targeted** to avoid more than one guard aiming for the same node.

## Fuzzy Logic
```
.
+-- _scenes
| 	+-- fuzzy
|	|	+-- fuzzy_module_danger.tscn
|	|	+-- fuzzy_variable_danger.tscn
|	|	+-- fuzzy_variable_direction.tscn
|	|	+-- fuzzy_variable_distance.tscn
| 	+-- templates
|	|	+-- fuzzy_module.tscn
|	|	+-- fuzzy_rule.tscn
|	|	+-- fuzzy_variable.tscn
|	|	+-- fuzzy_set.tscn
|   +-- guard.tscn
+-- _scripts
| 	+-- goals
|	|	+-- guard
|	|	|	+-- goal_keep_formation.gd
| 	+-- templates
|	|	+-- fuzzy_and.gd
|	|	+-- fuzzy_module.gd
|	|	+-- fuzzy_or.gd
|	|	+-- fuzzy_rule.gd
|	|	+-- fuzzy_set.gd
|	|	+-- fuzzy_proxy.gd
|	|	+-- fuzzy_term.gd
|	|	+-- fuzzy_variable.gd
```
### Template
Fuzzy logic was implemented in the following hierarchy:

```mermaid
graph TB
A((Module)) --> B((Rules))
A --> J((Variables))
J --> K((Variable))
J --> L((Variable))
L --> N((Set))
L --> O((Set))
L --> P((Set))
K --> M(...)
D --> G(...)
B --> C((Rule))
B --> D((Rule))
C --> E((Antecedent))
C --> F((Consequence))
E --> H((Term))
F --> I((Term))
```
*Fuzzy Module*
The fuzzy module is the one responsible to hold all the fuzzy variables and rules and perform operations of fuzzify and defuzzify on it. To **fuzzify** a *variable* is to calculate the **Degree of Memebership (DoM)** of each *set* for a determined value. To **defuzzify** a variable is to estimate a **value** corresponded to a fuzzified *variable* resulted from the calculation of the *rules* of the *module*.

*Fuzzy Variable*
A fuzzy variable holds many *fuzzy sets* and can **fuzzify**, that is, calculate their Degree of Membership (DoM) according to a value, and **defuzzify**, that is, calculate a value according to the DoM of it's sets. Currently only the Max Average method of defuzzification is implemented

*Fuzzy Set*
A fuzzy set is a **mathematical function** that takes a value and results in a Degree of Membership (DoM). Currently only Left Sholder, Triangle and Right Shoulder are implemented.

*Fuzzy Rule*
A fuzzy rule basically apply the Degree of Membership (DoM) of an antecedent *fuzzy term* to a consequent *fuzzy term*.

*Fuzzy Term*
A fuzzy term is an operation involving fuzzy sets. Currently implemented terms are a Set Proxy, AND and OR operations.

### In Shadow Dancer
Fuzzy logic is used to determine how the guard should approach an intruder in the [Keep Formation](#atomic-goals) Goal.

*Fuzzy Module Danger*
The module receives the *distance* between the guard and intruder position, and the angle (cosine value) between their *heading directions*, and then calculate the the **danger** value.

*Fuzzy Variable Distance*
The range of the value is 0 to 200 and it is represented by three sets, Close (Left Shoulder), Medium (Triangle) and Far (Right Shoulder).

*Fuzzy Variable Direction*
The range of the value is -1 to 1 and it is represented by three sets, Front(Left Shoulder), Side(Triangle) and Back(Right Shoulder).

*Fuzzy Variable Danger*
The range of the value is from 0 to 100. If the danger value is *low*, the guard will advance against the intruder. If *medium*, the guard will advance cautiously. And if *high*, the guard will retreat to the original formation point.

*Fuzzy rules*
The rules for this module are applied as follows (Vertical Axis = Direction, Horizontal Axis = Distance, Result = Danger):

|       |       |       |       |
| :---: | :---: | :---: | :---: | 
| &     | **Close** | **Medium** | **Far** |
| **Front** | High  | High       | Medium  |
| **Side**  | Medium| Medium     | Low     |
| **Back**  | Low   | Low        | Low     |

##  Test files
```
.
+-- _scenes
| 	+-- tests
|	|	+-- fsm
|	|	+-- fuzzy
|	|	+-- goal_driven
|	|	+-- graph
|	|	+-- pathfinder
|	|	+-- perception
|	|	+-- steering
+-- _scripts
| 	+-- tests
|	|	+-- fsm
|	|	+-- fuzzy
|	|	+-- goal_driven
|	|	+-- pathfinder
|	|	+-- perception
|	|	+-- steering
```
Several test scenes and scripts were created to test individual components of the project and can be found in the folders as described in the diagram above.

## Video Demo
To visualize the guards behaviour, two video demos are available, one without and one with collision shapes visible. Note that the player character is the shadowy figure and it has the ability to perform a dash to speed up and avoid the guards.

**Demo**
![Demo Video](demo.mp4)

**Demo with Collision Shapes**
![Demo With Collision Video](demo-with-collision.mp4)




> Written with [StackEdit](https://stackedit.io/).
